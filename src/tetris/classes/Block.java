/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class Block implements Cloneable
{
    //////////////////////////////////////////////////
    // Data Members
    
    private Point2d mPosition;
    private BufferedImage mBufferedImage;
    
    
    
    //////////////////////////////////////////////////
    // Constructor
    
    public Block()
    {
        init();
    }
    
    public Block( Point2d position )
    {
        init();
        
        setPosition( position );
    }
    
    public Block( Point2d position , BufferedImage bufferedImage )
    {
        init();
        
        setPosition( position );
        setBufferedImage( bufferedImage );
    }
    
    public Block( Point2d position , String image )
    {
        init();
        
        setPosition( position );
        setBufferedImage( image );
    }
    
    public Block( int x , int y )
    {
        init();
        
        setRelativePosition( x , y );
    }
    
    public Block( int x , int y , BufferedImage bufferedImage )
    {
        init();
        
        setRelativePosition( x , y );
        setBufferedImage( bufferedImage );
    }
    
    public Block( int x , int y , String image )
    {
        init();
        
        setRelativePosition( x , y );
        setBufferedImage( image );
    }
    
    
    
    // This method will initialize all data members
    //
    private void init()
    {
        mPosition = new Point2d( 0 , 0 );
        
        mBufferedImage = null;
    }
    
    
    //////////////////////////////////////////////////
    // Getter Methods
    
    // This method will return the position of the block
    //
    public Point2d getPosition()
    {
        return mPosition;
    }
    
    // This method will return the block's image
    //
    public BufferedImage getBufferedImage()
    {
        return mBufferedImage;
    }
    
    
    
    //////////////////////////////////////////////////
    // Setter Methods
    
    // This method will set the position of the current block
    //
    public void setPosition( Point2d position )
    {
        mPosition = position;
    }
    
    // This method will set the position of the current block
    //
    public void setRelativePosition( int x , int y )
    {
        mPosition.x = x;
        mPosition.y = y;
    }
    
    // This method will set the buffered image of the block
    //
    public void setBufferedImage( BufferedImage bufferedImage )
    {
        mBufferedImage = bufferedImage;
    }
    
    // This method will set the buffered image of the block
    //
    public void setBufferedImage( String image )
    {
        try
        {
            // NetBeans implementation of reading images in a frustrating manner
            mBufferedImage = ImageIO.read( getClass().getClassLoader().getResource( image ) );
        } catch ( Exception e )
        {
        }
    }
    
    
    
    //////////////////////////////////////////////////
    // Cloneable Methods
    
    @Override
    public Object clone() throws CloneNotSupportedException 
    {
        Block clone = (Block)super.clone();
        
        // Clone non-primitive type
        clone.setPosition( (Point2d)mPosition.clone() );
        
        return clone;
    }
}
