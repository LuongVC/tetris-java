/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

import java.util.Random;

public class TetrisBlockGenerator
{
    //////////////////////////////////////////////////
    // Enumeration
    
    public enum Shape
    {
        LINE_BLOCK,
        SQUARE_BLOCK,
        T_BLOCK,
        S_BLOCK, Z_BLOCK,
        L_BLOCK, REVERSE_L_BLOCK
    }
    
    //////////////////////////////////////////////////
    // generator
    
    // This method will return a random block shape
    //
    public static BaseShape createRandomBlock()
    {
        // Get the range of the shape list
        int min = 0;
        int max = 0;
        for( Shape s : Shape.values() )
        {
            min = Math.min( min , s.ordinal() );
            max = Math.max( max , s.ordinal() );
        }
        
        // Generate a random number between min and max values
        Random random = new Random();
        int number = random.nextInt( max - min +1 ) + min;
        
        // Create a shape block base on the number value and return it   
        return createSpecificBlock( Shape.values()[number] );
    }
    
    // This method will return a specific block shape
    //
    public static BaseShape createSpecificBlock( Shape type )
    {
        BaseShape shape = null;
        
        // Create a shape block base on the number value
        switch( type )
        {
            case LINE_BLOCK:
                shape = new LineShape();
                break;
            case SQUARE_BLOCK:
                shape = new SquareShape();
                break;
            case T_BLOCK:
                shape = new TShape();
                break;
            case S_BLOCK:
                shape = new SShape();
                break;
            case Z_BLOCK:
                shape = new ZShape();
                break;
            case L_BLOCK:
                shape = new LShape();
                break;
            case REVERSE_L_BLOCK:
                shape = new ReverseLShape();
                break;
        }
        
        return shape;
    }
}
