/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class BaseShape implements Cloneable
{
    //////////////////////////////////////////////////
    // Data Members
    
    private String mDefaultImage;
    private Point2d mPosition;
    private List<Block> mBlocks;
    private Bound mBound;
    
    
    
    //////////////////////////////////////////////////
    // Constructor
    
    public BaseShape()
    {
        init();
        realignBlocks();
        
        setBufferedImage( mDefaultImage );
    }
    
    public BaseShape( Point2d position )
    {
        init();
        realignBlocks();
        
        setPosition( position );
        setBufferedImage( mDefaultImage );
    }
    
    public BaseShape( Point2d position , BufferedImage bufferedImage )
    {
        init();
        realignBlocks();
        
        setPosition( position );
        setBufferedImage( bufferedImage );
    }
    
    public BaseShape( Point2d position , String image )
    {
        init();
        realignBlocks();
        
        setPosition( position );
        setBufferedImage( image );
    }
    
    public BaseShape( int x , int y )
    {
        init();
        realignBlocks();
        
        setPosition( x , y );
        setBufferedImage( mDefaultImage );
    }
    
    public BaseShape( int x , int y , BufferedImage bufferedImage )
    {
        init();
        realignBlocks();
        
        setPosition( x , y );
        setBufferedImage( bufferedImage );
    }
    
    public BaseShape( int x , int y , String image )
    {
        init();
        realignBlocks();
        
        setPosition( x , y );
        setBufferedImage( image );
    }
    
    // This method will initialize the data members
    //
    protected void init()
    {
        mDefaultImage = "resources/Block.png";
        
        mPosition = new Point2d();
        mPosition.x = 0;
        mPosition.y = 0;
        
        mBlocks = createShape();
        mBound = createBound();
    }
    
    // This method will create the shape of the block
    //
    protected List<Block> createShape()
    {
        List<Block> blocks = new ArrayList<>();
        blocks.add( new Block( 0 , 0 ) );
        
        return blocks;
    }
    
    // This method will calculate and setup the bound of the shape
    //
    protected Bound createBound()
    {
        Bound bound = null;
        
        if( mBlocks != null )
        {
            bound = new Bound();
            updateBound( mBlocks , bound );
        }
        
        return bound;
    }
    
    
    
    //////////////////////////////////////////////////
    // Getter Methods
    
    // This method will return the position of the shape
    //
    public Point2d getPosition()
    {
        return mPosition;
    }
    
    // This method will set the buffered image of the shape
    //
    public BufferedImage getBufferedImage()
    {
        BufferedImage bufferedImage = null;
        
        // Check if the shape is made up of any blocks
        if( mBlocks != null && mBlocks.size() > 0 )
        {
            // Get the buffer image from the block
            bufferedImage = mBlocks.get( 0 ).getBufferedImage();
        }
        
        return bufferedImage;
    }
    
    // This method will return the blocks of the shape
    //
    public List<Block> getBlocks()
    {
        return mBlocks;
    }
    
    // This method will return the bound of the shape
    //
    public Bound getBound()
    {
        return mBound;
    }
    
    // This method will return the width of the shape bound
    //
    public int getWidth()
    {
        int width = mBound.right - mBound.left;
        width = ( width < 0 ) ? -width : width;
        width++;
        
        return width;
    }
    
    // This method will return the height of the shape bound
    //
    public int getHeight()
    {
        int height = mBound.bottom - mBound.top;
        height = ( height < 0 ) ? -height : height;
        height++;
        
        return height;
    }
    
    
    //////////////////////////////////////////////////
    // Setter Methods
    
    // This method will set the position of the shape
    //
    public void setPosition( Point2d position )
    {
        mPosition = position;
    }
    
    // This method will set the position of the shape
    //
    public void setPosition( int x , int y )
    {
        mPosition.x = x;
        mPosition.y = y;
    }
    
    // This method will set the buffered image of the shape
    //
    public void setBufferedImage( BufferedImage bufferedImage )
    {
        if( mBlocks != null )
        {
            // Loop through all the blocks within the shape and set the image
            for ( Block block : mBlocks )
            {
                block.setBufferedImage( bufferedImage );
            }
        }
    }
    
    // This method will set the buffered image of the shape
    //
    public void setBufferedImage( String image )
    {
        if( mBlocks != null )
        {
            // Loop through all the blocks within the shape and set the image
            BufferedImage temporaryBufferedImage = null;
            for ( Block block : mBlocks )
            {
                if( temporaryBufferedImage == null )
                {
                    // Set the buffered image for the block
                    block.setBufferedImage( image );
                    
                    // Store a temporary variable to reuse the new buffer image
                    temporaryBufferedImage = block.getBufferedImage();
                }
                else
                {
                    // Set the buffered image for the block
                    block.setBufferedImage( temporaryBufferedImage );
                }
            }
        }
    }
    
    
    
    //////////////////////////////////////////////////
    // Cloneable Methods
    
    @Override
    public Object clone() throws CloneNotSupportedException 
    {
        BaseShape clone = (BaseShape)super.clone();
        
        // Cloning non-primitive types
        clone.mBound = (Bound)mBound.clone();
        clone.mPosition = (Point2d)mPosition.clone();
        
        // Cloning blocks
        clone.mBlocks = new ArrayList<>( mBlocks.size() );
        for( Block block : mBlocks )
        {
            clone.getBlocks().add( (Block)block.clone() );
        }
        
        return clone;
    }
    
    
    
    //////////////////////////////////////////////////
    // Protected Methods
    
    // This method will update the bound/dimension of the shape
    //
    protected void updateBound( List<Block> blocks , Bound bound )
    {
        if( blocks != null && bound != null )
        {
            // Reset the boudning area of the shape
            bound.clearBound();
            
            // Find the bounding area of the shape
            for( Block block : blocks )
            {
                int posX = block.getPosition().x;
                bound.left = ( bound.left < posX ) ? bound.left : posX;
                bound.right = ( bound.right > posX ) ? bound.right : posX;
                
                int posY = block.getPosition().y;
                bound.top = ( bound.top < posY ) ? bound.top : posY;
                bound.bottom = ( bound.bottom > posY ) ? bound.bottom : posY;
            }
        }
    }
    
    // This method will realign the shape's block to the origin
    //
    protected void realignBlocks()
    {
        if( mBlocks != null && mBound != null )
        {
            for( Block block : mBlocks )
            {
                // Align it to the origin
                block.getPosition().x -= mBound.left;
                block.getPosition().y -= mBound.top;
                
                // Shift center the shape
                block.getPosition().x -= ( getWidth() / 2 );
                block.getPosition().y -= ( getHeight() / 2 );
            }

            // Update the bounding area
            updateBound( mBlocks , mBound );
        }
    }
    
    //////////////////////////////////////////////////
    // Public Methods
    
    // This method will translate the block's position
    //
    public void translate( int x , int y )
    {
        mPosition.x += x;
        mPosition.y += y;
    }
    
    // This method will rotate the block shape clockwise
    //
    public void rotateCW()
    {
        // Loop through the blocks in the shape
        for ( Block block : mBlocks )
        {
            // Calculate the change
            int x = block.getPosition().y * -1;
            int y = block.getPosition().x;
            
            // Update the block new position
            block.setRelativePosition( x , y );
        }
        
        // Update the bounding area
        updateBound( mBlocks , mBound );
        
        // Realign the block
        realignBlocks();
    }
    
    // This method will rotate the block shape counter-clockwise
    //
    public void rotateCCW()
    {
        // Loop through the blocks in the shape
        for ( Block block : mBlocks )
        {
            // Calculate the change
            int x = block.getPosition().y;
            int y = block.getPosition().x * -1;
            
            // Update the block new position
            block.setRelativePosition( x , y );
        }
        
        // Update the bounding area
        updateBound( mBlocks , mBound );
        
        // Realign the block
        realignBlocks();
    }
}
