/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class LShape extends BaseShape
{
    //////////////////////////////////////////////////
    // Constructors
    
    public LShape()
    {
        super();
    }
    
    public LShape( Point2d position )
    {
        super( position );
    }
    
    public LShape( Point2d position , BufferedImage bufferedImage )
    {
        super( position , bufferedImage );
    }
    
    public LShape( Point2d position , String image )
    {
        super( position , image );
    }
    
    public LShape( int x , int y )
    {
        super( x , y );
    }
    
    public LShape( int x , int y , BufferedImage bufferedImage )
    {
        super( x , y , bufferedImage );
    }
    
    public LShape( int x , int y , String image )
    {
        super( x , y , image );
    }
    
    
    
    //////////////////////////////////////////////////
    // Override Methods
    
    // This method will create the shape of the block
    //
    @Override
    public List<Block> createShape()
    {
        List<Block> blocks = new ArrayList<>();
        blocks.add( new Block( 0 , 0 ) );
        blocks.add( new Block( 0 , 1 ) );
        blocks.add( new Block( 0 , 2 ) );
        blocks.add( new Block( 1 , 2 ) );
        
        return blocks;
    }
}
