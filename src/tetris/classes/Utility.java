/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Luong
 */
public class Utility
{
    public Utility()
    {
        
    }
    
    public BufferedImage CreateBufferedImage( String filepath )
    {
        BufferedImage bufferedImage = null;
        
        try
        {
            // NetBeans implementation of reading images
            bufferedImage = ImageIO.read( getClass().getClassLoader().getResource( filepath ) );
        }
        catch( IOException ex )
        {
            // Failed to create the buffered image
            bufferedImage = null;
            
            // Log the error
            Logger.getLogger( Utility.class.getName() ).log( Level.SEVERE , null , ex );
        }
        
        return bufferedImage;
    }
}
