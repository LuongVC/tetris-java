/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

/**
 *
 * @author Luong
 */
public class Bound implements Cloneable
{
    public int left;
    public int right;
    public int top;
    public int bottom;
    
    //////////////////////////////////////////////////
    // Constructors
    
    public Bound()
    {
        init();
    }
    
    public void init()
    {
        left = 0;
        right = 0;
        top = 0;
        bottom = 0;
    }
    
    
    //////////////////////////////////////////////////
    // Getter Methods
    
    // This method will return the width of the bounding area
    // 
    public int getWidth()
    {
        int width = right - left;
        width = ( width >= 0 ) ? width : -width;
        
        return width;
    }
    
    // This method will return the height of the bounding area
    // 
    public int getHeight()
    {
        int height = bottom - top;
        height = ( height >= 0 ) ? height : -height;
        
        return height;
    }
    
    
    
    //////////////////////////////////////////////////
    // Override Methods
    
    // This method will clear the bounding area to default value
    //
    public void clearBound()
    {
        init();
    }
    
    
    
    //////////////////////////////////////////////////
    // Cloneable Methods
    
    @Override
    public Object clone() throws CloneNotSupportedException 
    {
        return (Bound)super.clone();
    }
}
