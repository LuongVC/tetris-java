/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

/**
 *
 * @author Luong
 */
public class Point2d implements Cloneable
{
    public int x;
    public int y;
    
    public Point2d()
    {
        this.x = 0;
        this.y = 0;
    }

    public Point2d( int x , int y )
    {
        this.x = x;
        this.y = y;
    }
    
    
    
    //////////////////////////////////////////////////
    // Cloneable Methods
    
    @Override
    public Object clone() throws CloneNotSupportedException 
    {
        return (Point2d)super.clone();
    }
}
