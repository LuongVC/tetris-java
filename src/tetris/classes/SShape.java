/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class SShape extends BaseShape
{
    //////////////////////////////////////////////////
    // Constructors
    
    public SShape()
    {
        super();
    }
    
    public SShape( Point2d position )
    {
        super( position );
    }
    
    public SShape( Point2d position , BufferedImage bufferedImage )
    {
        super( position , bufferedImage );
    }
    
    public SShape( Point2d position , String image )
    {
        super( position , image );
    }
    
    public SShape( int x , int y )
    {
        super( x , y );
    }
    
    public SShape( int x , int y , BufferedImage bufferedImage )
    {
        super( x , y , bufferedImage );
    }
    
    public SShape( int x , int y , String image )
    {
        super( x , y , image );
    }
    
    
    
    //////////////////////////////////////////////////
    // Override Methods
    
    // This method will create the shape of the block
    //
    @Override
    public List<Block> createShape()
    {
        List<Block> blocks = new ArrayList<>();
        blocks.add( new Block( 1 , 0 ) );
        blocks.add( new Block( 2 , 0 ) );
        blocks.add( new Block( 0 , 1 ) );
        blocks.add( new Block( 1 , 1 ) );
        
        return blocks;
    }
}
