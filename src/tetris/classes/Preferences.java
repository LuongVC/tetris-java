/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.classes;

/**
 *
 * @author Luong
 */
public class Preferences
{
    public static int GridColumn = 10;
    public static int GridRow = 24;
    
    public static int DefaultGameSpeed = 800; // 800ms
    public static int MaxGameSpeed = 50; // 50ms
    
    public static int PointsPerRow = 100;
    
    public static int SidePanelWidth = 250;
}
