/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import java.awt.Graphics;
import javax.swing.JPanel;
import tetris.classes.BaseShape;
import tetris.classes.Block;

public class TetrisShapePanel extends JPanel
{
    //////////////////////////////////////////////////
    // Data Member
    
    BaseShape mBaseShape;
    
    
    
    //////////////////////////////////////////////////
    // Constructor
    
    public TetrisShapePanel()
    {
        init();
    }
    
    public TetrisShapePanel( BaseShape shape )
    {
        init();
        
        mBaseShape = shape;
    }
    
    

    //////////////////////////////////////////////////
    // Constructor Helper
    
    private void init()
    {
        mBaseShape = null;
    }
    
    
    
    //////////////////////////////////////////////////
    // Draw Methods
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);     
        
        drawShape( g , mBaseShape );
    }
    
    // This method will draw the current shape
    //
    private void drawShape( Graphics g , BaseShape shape )
    {
        if( shape != null )
        {
            // Loop through each blocks of the shape
            for( Block block : shape.getBlocks() )
            {
                if( block.getBufferedImage() != null )
                {
                    // Calculate the position of the block onto the screen
                    int imageWidth = block.getBufferedImage().getWidth();
                    int imageHeight = block.getBufferedImage().getHeight();
                    
                    float centerX = ( getSize().width / 2.0f ) - ( ( shape.getWidth() * imageWidth ) / 2.0f );
                    int blockPosX = ( block.getPosition().x - shape.getBound().left ) * imageWidth;
                    
                    int posX = (int)centerX + blockPosX;
                    int posY = ( block.getPosition().y - shape.getBound().top ) * imageHeight;;

                    // Draw the block
                    g.drawImage( block.getBufferedImage() , 
                            posX , posY , 
                            imageWidth , imageHeight , 
                            this );
                }
            }
        }
    }
    
    
    
    //////////////////////////////////////////////////
    // Setter Methods
    
    public void setShape( BaseShape shape )
    {
        // Set the shape
        mBaseShape = shape;
        
        // Redraw the shape
        repaint();
    }
}
