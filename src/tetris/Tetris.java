/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import javax.swing.SwingUtilities;

public class Tetris 
{
    //////////////////////////////////////////////////
    // Main
    
    public static void main(String[] args) 
    {
        // Create the UI
        SwingUtilities.invokeLater(new Runnable() 
        {
            @Override
            public void run()
            {
                TetrisGame game = new TetrisGame();
                game.showWindow();
            }
        });
    }
}
