/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import tetris.classes.BaseShape;
import tetris.classes.Block;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;
import tetris.classes.Point2d;
import tetris.classes.Preferences;
import tetris.classes.TetrisBlockGenerator;
import tetris.classes.Utility;


public class TetrisGamePanel extends JPanel
        implements ActionListener
{
    //////////////////////////////////////////////////
    // Enumeration

    public enum Status
    {
        INVALID,
        ACTION_OK,
        OUTSIDE_OF_LEFT_BOUND,
        OUTSIDE_OF_RIGHT_BOUND,
        REACH_BOTTOM,
        NO_COLLISION,
        COLLISION,
        COLLIDED_WITH_SIDE,
        COLLIDED_WITH_LEFT_SIDE,
        COLLIDED_WITH_RIGHT_SIDE,
        COLLIDED_WITH_TOP,
        COLLIDED_WITH_BELOW
    }

    public enum Rotation
    {
        CLOCKWISE,
        COUNTER_CLOCKWISE
    }

    //////////////////////////////////////////////////
    // Private Members
    
    private TetrisGame mCallback;

    private int mGridWidth;
    private int mGridHeight;
    private Block[][] mGrid;
    
    private String mHelperBlockImage;
    
    private String mGameTitle;
    private String mStartInstruction;
    private int mStartInstructionSize;
    
    private BufferedImage mTitlescreenImage;
    private BufferedImage mBackgroundImage;
    private BufferedImage mImagePause;
    private BufferedImage mImageGameOver;

    private int mImageBlockWidth;
    private int mImageBlockHeight;

    private int mSpeed;
    private Timer mTimer;

    private BaseShape mCurrentShape;
    private BaseShape mHelperShape;
    private Status mShapeStatus;

    private int mNumOfLineCleared;

    private boolean bIsGameStarted;
    private boolean bIsGamePaused;
    private boolean bIsGameOver;

    //////////////////////////////////////////////////
    // Constructor
    public TetrisGamePanel()
    {
        init();
    }

    public TetrisGamePanel( int width , int height )
    {
        init();

        mGridWidth = width;
        mGridHeight = height;
    }

    
    
    //////////////////////////////////////////////////
    // Constructor Helper
    
    // This method will initialize the data members
    //
    private void init()
    {
        mCallback = null;

        mGridWidth = 10;
        mGridHeight = 24;
        mGrid = new Block[ mGridWidth ][ mGridHeight ];

        mImageBlockWidth = 32;
        mImageBlockHeight = 32;

        mHelperBlockImage = "resources/Guide.png";
        
        mGameTitle = "Tetris";
        mStartInstruction = "Press Any Key To Start";
        mStartInstructionSize = 24;
        
        Utility utility = new Utility();
        mTitlescreenImage = utility.CreateBufferedImage( "resources/Titlescreen.png" );
        mBackgroundImage = utility.CreateBufferedImage( "resources/Game_Panel_Background.png" );
        mImageGameOver = utility.CreateBufferedImage( "resources/GameOver.png" );
        mImagePause = utility.CreateBufferedImage( "resources/Pause.png" );

        mSpeed = Preferences.DefaultGameSpeed;
        mTimer = null;

        mCurrentShape = null;
        mShapeStatus = Status.INVALID;

        mNumOfLineCleared = 0;

        bIsGameStarted = false;
        bIsGamePaused = false;
        bIsGameOver = false;
    }

    
    
    //////////////////////////////////////////////////
    // Override Methods
    
    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension( getWidth() , getHeight() );
    }

    // This is called by the timer
    @Override
    public void actionPerformed( ActionEvent ae )
    {
        if ( mCurrentShape != null && !bIsGamePaused && !bIsGameOver )
        {
            switch ( mShapeStatus )
            {
                case REACH_BOTTOM:
                case COLLIDED_WITH_BELOW:
                    // If the block has collide, record the block
                    mShapeStatus = recordBlocks( mCurrentShape );

                    // Record the number of lines removed
                    mNumOfLineCleared += removeAllLineFilled();
                    accumulateScore();

                    // Create a new block
                    requestNewShape();
                    break;
                default:
                    // If there is no problem, move the shape
                    mShapeStatus = moveShape( mCurrentShape , 0 , 1 );
                    break;
            }
        }

        // Redraw the screen
        repaint();
    }

    @Override
    public void paintComponent( Graphics g )
    {
        super.paintComponent( g );

        //drawBackgroundImage( g , mBackgroundImage );
        
        if( !bIsGameStarted )
        {
            drawTitlescreenImage( g , mTitlescreenImage );
        }
        else
        {
            drawGridBlocks( g , mGrid );
            drawShape( g , mHelperShape );
            drawShape( g , mCurrentShape );
        }

        if ( bIsGameOver )
        {
            drawGameOverText( g );
        } else if ( bIsGamePaused )
        {
            drawPauseText( g );
        }
    }

    
    
    //////////////////////////////////////////////////
    // Timer Methods
    
    // This method will reset the timer
    //
    private void restartTimer()
    {
        // Stop the timer. Timer is only pause and not reset
        if ( mTimer != null )
        {
            mTimer.stop();
            mTimer = null;
        }

        // Create a new timer
        mTimer = new Timer( mSpeed , this );
        mTimer.start();
    }

    
    
    //////////////////////////////////////////////////
    // Draw Methods
    
    // This method will draw the background image
    //
    private void drawTitlescreenImage( Graphics g , BufferedImage bufferedImage )
    {
        // Resize the image
        int widthToHeightRatio = bufferedImage.getHeight() / bufferedImage.getWidth();
        int imageWidth = getWidth();
        int imageHeight = imageWidth * widthToHeightRatio;
        Image resizeImage = bufferedImage.getScaledInstance( imageWidth , imageHeight , Image.SCALE_SMOOTH );
        
        // Draw the titlescreen image
        g.drawImage( resizeImage ,
                0 , 150 , imageWidth , imageHeight ,
                this );
        
        // Store the current font
        Font prevFont = g.getFont();
        
        // Draw the game title
        g.setFont( new Font( prevFont.getFontName() , Font.BOLD , 96 ) );
        g.drawString( mGameTitle , 25 , 200 );
        
        // Draw the instructions
        g.setFont( new Font( prevFont.getFontName() , Font.BOLD , mStartInstructionSize ) );
        g.drawString( mStartInstruction , 25 , 600 );
        
        // Restore the previous font
        g.setFont( prevFont );
    }
    
    // This method will draw the background image
    //
    private void drawBackgroundImage( Graphics g , BufferedImage bufferedImage )
    {
        g.drawImage( bufferedImage ,
                0 , 0 ,
                getWidth() , getHeight() ,
                this );
    }

    // This method will draw the current shape
    //
    private void drawShape( Graphics g , BaseShape shape )
    {
        if ( shape != null )
        {
            // Loop through each blocks of the shape
            for ( Block block : shape.getBlocks() )
            {
                // Calculate the position of the block onto the screen
                int posX = ( block.getPosition().x + shape.getPosition().x ) * mImageBlockWidth;
                int posY = ( block.getPosition().y + shape.getPosition().y ) * mImageBlockHeight;

                // Draw the block
                g.drawImage( block.getBufferedImage() ,
                        posX , posY ,
                        mImageBlockWidth , mImageBlockHeight ,
                        this );
            }
        }
    }

    // This method will draw all the blocks within the grid
    //
    private void drawGridBlocks( Graphics g , Block[][] grid )
    {
        // Loop through the x-axis (column) of the screen
        for ( int x = 0 ; x < mGridWidth ; x++ )
        {
            // Loop through the y-axis (row) of the screen
            for ( int y = 0 ; y < mGridHeight ; y++ )
            {
                // Check if there is a block in the cooresponding grid location
                if ( mGrid[ x ][ y ] != null )
                {
                    // Draw the block
                    g.drawImage( mGrid[ x ][ y ].getBufferedImage() ,
                            x * mImageBlockWidth , y * mImageBlockHeight ,
                            mImageBlockWidth , mImageBlockHeight ,
                            this );
                }
            }
        }
    }

    // This method will draw the Game Over text on the screen
    //
    private void drawGameOverText( Graphics g )
    {
        if ( mImageGameOver != null )
        {
            // Calculate the center position
            int posX = ( mGridWidth / 2 * mImageBlockWidth ) - ( mImageGameOver.getWidth() / 2 );
            int posY = ( mGridHeight / 3 * mImageBlockWidth ) - ( mImageGameOver.getHeight() / 2 );

            // Draw the Game Over image
            g.drawImage( mImageGameOver ,
                    posX , posY ,
                    mImageGameOver.getWidth() , mImageGameOver.getHeight() ,
                    this );
        }
    }

    // This method will draw the Game Over text on the screen
    //
    private void drawPauseText( Graphics g )
    {
        if ( mImagePause != null )
        {
            // Calculate the center position
            int posX = ( mGridWidth / 2 * mImageBlockWidth ) - ( mImagePause.getWidth() / 2 );
            int posY = ( mGridHeight / 3 * mImageBlockWidth ) - ( mImagePause.getHeight() / 2 );

            // Draw the Game Over image
            g.drawImage( mImagePause ,
                    posX , posY ,
                    mImagePause.getWidth() , mImagePause.getHeight() ,
                    this );
        }
    }

    
    
    //////////////////////////////////////////////////
    // Setter Methods
    
    public void setGridWidth( int width )
    {
        mGridWidth = width;
    }

    public void setGridHeight( int height )
    {
        mGridHeight = height;
    }

    public void setSpeed( int speed )
    {
        mSpeed = speed;
    }

    public void setCallback( TetrisGame callback )
    {
        mCallback = callback;
    }
    
    public void setStartInstruction( String instruction , int fontSize )
    {
        mStartInstruction = instruction;
        mStartInstructionSize = fontSize;
    }

    
    
    //////////////////////////////////////////////////
    // Getter Methods
    
    @Override
    public int getWidth()
    {
        return mGridWidth * mImageBlockWidth;
    }

    @Override
    public int getHeight()
    {
        return mGridHeight * mImageBlockHeight;
    }

    public int getGridColumn()
    {
        return mGridWidth;
    }

    public int getGridRow()
    {
        return mGridHeight;
    }

    public int getSpeed()
    {
        return mSpeed;
    }
    
    public boolean isGameStarted()
    {
        return bIsGameStarted;
    }

    public boolean isGamePaused()
    {
        return bIsGamePaused;
    }

    public boolean isGameOver()
    {
        return bIsGameOver;
    }

    
    
    //////////////////////////////////////////////////
    // Block Methods
    
    // This method will request for a new shape
    //
    private void requestNewShape()
    {
        // Get a new shape
        if ( mCallback != null )
        {
            mCurrentShape = mCallback.onGetNextShape();
        } else
        {
            // Setup the shape for the grid
            mCurrentShape = TetrisBlockGenerator.createRandomBlock();
        }

        if ( mCurrentShape != null )
        {
            // Realign the shape within the grid
            float centerPos = ( mGridWidth / 2.0f ) - ( mCurrentShape.getWidth() / 2.0f ) + 1.0f;
            mCurrentShape.setPosition( (int) centerPos , -mCurrentShape.getBound().top );

            // Check if there is a collision when new shape is added
            Status status = isMovementPossible( mCurrentShape , 0 , 0 );
            if ( status == Status.COLLISION )
            {
                bIsGameOver = true;
            } else
            {
                // Reset the status
                mShapeStatus = Status.INVALID;

                // Restart the timer
                restartTimer();

                // Create a copy of the block as a helper
                try
                {
                    mHelperShape = (BaseShape) mCurrentShape.clone();
                    mHelperShape.setBufferedImage( mHelperBlockImage );
                } catch ( CloneNotSupportedException ex )
                {
                    Logger.getLogger( TetrisGamePanel.class.getName() ).log( Level.SEVERE , null , ex );
                }
                
                // Update the helper block to be drawn on the surface of the 
                // grid blocks
                updateHelperPosition();
            }

            // Redraw the screen
            repaint();
        }
    }

    // This method will add the block to the grid and release the player control
    // of the shape
    //
    private Status recordBlocks( BaseShape shape )
    {
        // Stop the timer
        if ( mTimer != null )
        {
            mTimer.stop();
        }

        // Loop through each blocks of the shape
        for ( Block block : shape.getBlocks() )
        {
            // Calculate the position of the block
            int posX = block.getPosition().x + shape.getPosition().x;
            int posY = block.getPosition().y + shape.getPosition().y;

            // Store the block in the cooresponding grid location
            mGrid[ posX ][ posY ] = block;
        }

        // Release the current shape
        mCurrentShape = null;

        // Release the timer
        if ( mTimer != null )
        {
            mTimer = null;
        }

        return Status.INVALID;
    }

    // This method will send the number of row cleared and reset it
    //
    private void accumulateScore()
    {
        mCallback.onAccumlateScore( mNumOfLineCleared );

        mNumOfLineCleared = 0;
    }

    // This method will remove all the blocks in the grid
    //
    private void resetGrid()
    {
        // Loop through each of the lines
        for ( int y = 0 ; y < mGridHeight ; y++ )
        {
            // Loop through each of the blocks in the line
            for ( int x = 0 ; x < mGridWidth ; x++ )
            {
                // Remove the blcok
                mGrid[ x ][ y ] = null;
            }
        }
    }
    
    // This method will update the helper position
    //
    private void updateHelperPosition()
    {
        try
        {
            // Set the helper initial position to the current block position
            mHelperShape.setPosition( (Point2d)mCurrentShape.getPosition().clone() );
        } catch ( CloneNotSupportedException ex )
        {
            Logger.getLogger( TetrisGamePanel.class.getName() ).log( Level.SEVERE , null , ex );
        }
        
        // Move the shape down until it collide with a surface block
        Status status = Status.INVALID;
        do
        {
            status = moveShape( mHelperShape , 0 , 1 );
        } while ( status == Status.ACTION_OK );
    }

    
    
    //////////////////////////////////////////////////
    // Line Methods
    
    // This method will count the number of lines removed
    //
    private int removeAllLineFilled()
    {
        int numOfLinesCleared = 0;

        // Find the first solid line
        int linePos = findFirstLineFilled();

        // Perform the following action until there is no more 
        // solid lines
        while ( linePos != -1 )
        {
            // Remove the line and shift all blocks above down
            removeSingleLine( linePos );
            shiftBlocksAboveDownOne( linePos );

            // Tally the lines removed
            numOfLinesCleared++;

            // Find the next solid lines
            linePos = findNextLineFilled( linePos + 1 );
        }

        return numOfLinesCleared;
    }

    // This method will find the first row that is filled
    //
    private int findFirstLineFilled()
    {
        return findNextLineFilled( mGridHeight );
    }

    // This method will find the next row from the offset that is filled
    //
    private int findNextLineFilled( int position )
    {
        int index = -1;

        // Make sure the position is not higher than the screen height
        position = ( position >= mGridHeight ) ? mGridHeight - 1 : position;

        // Loop through each line starting from the buttom moving up
        for ( int y = position ; y > 0 && y != -1 ; y-- )
        {
            // Check if the line is fill
            if ( isLineFilled( y ) )
            {
                // Set the index and end loop
                index = y;
            }
        }

        return index;
    }

    // This method will check if there is a solid line at the specified posY
    //
    private boolean isLineFilled( int posY )
    {
        boolean bIsLineFilled = true;

        // Loop through all the blocks within the posY row
        for ( int x = 0 ; x < mGridWidth ; x++ )
        {
            // Check if there is any empty blocks
            if ( mGrid[ x ][ posY ] == null )
            {
                // There is an empty so, so it is not a solid line
                bIsLineFilled = false;
            }
        }

        return bIsLineFilled;
    }

    // This method will remove the solid line at the specified posY
    //
    private void removeSingleLine( int posY )
    {
        // Loop through all the blocks within the posY row
        for ( int x = 0 ; x < mGridWidth ; x++ )
        {
            // Remove the blocks
            mGrid[ x ][ posY ] = null;
        }
    }

    // This method will shift all blocks above the specified posY down one row
    //
    private void shiftBlocksAboveDownOne( int posY )
    {
        // Loop through all the lines in the grid starting at posY
        for ( int y = posY ; y > 0 ; y-- )
        {
            // Loop through all the blocks on the line y
            for ( int x = 0 ; x < mGridWidth ; x++ )
            {
                // Shift block down
                mGrid[ x ][ y ] = mGrid[ x ][ y - 1 ];
            }
        }

        // Clear the top line
        for ( int x = 0 ; x < mGridWidth ; x++ )
        {
            // Shift block down
            mGrid[ x ][ 0 ] = null;
        }
    }

    
    
    //////////////////////////////////////////////////
    // Movement Methods

    // This method will check if the movement is possible, and why
    // A movement of (x,y)=(0,0) can also be use to check for collsion
    //
    private Status isMovementPossible( BaseShape shape , int x , int y )
    {
        Status status = Status.INVALID;

        if ( shape != null )
        {
            // Loop through all the blocks that makes up the shape
            for ( Block block : shape.getBlocks() )
            {
                // Calculate the position of the block
                int posX = block.getPosition().x + shape.getPosition().x + x;
                int posY = block.getPosition().y + shape.getPosition().y + y;

                // Check if the block is outside the left side of screen
                if ( posX < 0 )
                {
                    status = Status.OUTSIDE_OF_LEFT_BOUND;
                }

                // Check if the block is outside the right side of screen
                if ( posX >= mGridWidth )
                {
                    status = Status.OUTSIDE_OF_RIGHT_BOUND;
                } // Check if the block has reach the bottom of the screen
                else if ( posY >= mGridHeight )
                {
                    status = Status.REACH_BOTTOM;
                } // Check if there is an exising block at the position
                else if ( posX >= 0 && posX < mGridWidth
                        && posY >= 0 && posY < mGridHeight
                        && mGrid[ posX ][ posY ] != null )
                {
                    status = Status.COLLISION;
                }
            }

            // If the status still haven't change, then there was no problem
            if ( status == Status.INVALID )
            {
                status = Status.ACTION_OK;
            }
        }

        return status;
    }

    // This method will move the shape
    //
    private Status moveShape( BaseShape shape , int x , int y )
    {
        Status status = Status.INVALID;

        if ( shape != null )
        {
            // Check if movement is possible
            status = isMovementPossible( shape , x , y );
            if ( status == Status.ACTION_OK )
            {
                // Move the shape
                shape.translate( x , y );
            } // Check if it is a collsion
            else if ( status == Status.COLLISION )
            {
                //Check if it is a collision with the side
                if ( x != 0 )
                {
                    status = Status.COLLIDED_WITH_SIDE;
                } // Or a collision with what's below
                else if ( y != 0 )
                {
                    status = Status.COLLIDED_WITH_BELOW;
                }
            }
        }

        // Redraw the block new position
        repaint();

        return status;
    }

    
    
    //////////////////////////////////////////////////
    // Rotation Methods
    
    // Check if it is possible to rotate the shape, and if not, why
    //
    private Status isRotationPossible( BaseShape shape )
    {
        Status status = Status.INVALID;

        // Go through all blocks within the shape
        for ( Block block : mCurrentShape.getBlocks() )
        {
            // Calculate the block position on the grid
            int posX = block.getPosition().x + mCurrentShape.getPosition().x;
            int posY = block.getPosition().y + mCurrentShape.getPosition().y;
            int leftSide = mCurrentShape.getBound().left + ( mCurrentShape.getWidth() / 2 );
            int rightSide = mCurrentShape.getBound().right - ( mCurrentShape.getWidth() / 2 );

            if ( posX >= 0 && posX < mGridWidth )
            {
                // Find all the block position to the left
                if ( block.getPosition().x <= leftSide )
                {
                    // Check if there is a collision with the left blocks
                    if ( mGrid[ posX ][ posY ] != null )
                    {
                        status = Status.COLLIDED_WITH_LEFT_SIDE;
                    }
                } // Find all the block position to the right
                else if ( block.getPosition().x >= rightSide )
                {
                    // Check if there is a collision with the right blocks
                    if ( mGrid[ posX ][ posY ] != null )
                    {
                        status = Status.COLLIDED_WITH_RIGHT_SIDE;
                    }
                } // Find all the lowest position block
                else if ( block.getPosition().y == mCurrentShape.getBound().bottom )
                {
                    // Check if there is a collision with the lowest position block
                    if ( mGrid[ posX ][ posY ] != null )
                    {
                        status = Status.COLLIDED_WITH_BELOW;
                    }
                } // Otherwise there is no collision
                else
                {
                    status = Status.NO_COLLISION;
                }
            } else if ( posX < 0 )
            {
                status = Status.OUTSIDE_OF_LEFT_BOUND;
            } else if ( posX >= mGridWidth )
            {
                status = Status.OUTSIDE_OF_RIGHT_BOUND;
            }
        }

        return status;
    }

    // This method will rotate the current shape clockwise
    //
    public void rotateShape( Rotation rotation , BaseShape shape )
    {
        Status status = Status.INVALID;

        if ( !bIsGamePaused && !bIsGameOver )
        {
            // Rotate the shape
            if ( rotation == Rotation.CLOCKWISE )
            {
                shape.rotateCW();
            } else
            {
                shape.rotateCCW();
            }

            // Calculate the halfway point
            int leftHalf = shape.getWidth() / 2;
            int rightHalf = shape.getWidth() - leftHalf;

            status = isRotationPossible( shape );
            switch ( status )
            {
                // Check if the shape rotation collide with any blocks below
                case COLLIDED_WITH_BELOW:
                    // Rotate back to the previous state
                    if ( rotation == Rotation.CLOCKWISE )
                    {
                        shape.rotateCW();
                    } else
                    {
                        shape.rotateCCW();
                    }
                    break;

                case OUTSIDE_OF_LEFT_BOUND:
                case COLLIDED_WITH_LEFT_SIDE:
                    // Attempt to shift the shape to the right
                    for ( int i = 1 ; i <= leftHalf && status != Status.ACTION_OK ; i++ )
                    {
                        status = isMovementPossible( shape , i , 0 );
                        if ( status == Status.ACTION_OK )
                        {
                            moveShape( shape , i , 0 );
                        }
                    }

                    // Failed to shift the shape to the right
                    if ( status != Status.ACTION_OK )
                    {
                        // Rotate back to the previous state
                        if ( rotation == Rotation.CLOCKWISE )
                        {
                            shape.rotateCCW();
                        } else
                        {
                            shape.rotateCW();
                        }
                    }
                    break;

                case OUTSIDE_OF_RIGHT_BOUND:
                case COLLIDED_WITH_RIGHT_SIDE:
                    // Attempt to shift the shape to the left
                    for ( int i = 1 ; i <= rightHalf && status != Status.ACTION_OK ; i++ )
                    {
                        status = isMovementPossible( shape , -i , 0 );
                        if ( status == Status.ACTION_OK )
                        {
                            moveShape( shape , -i , 0 );
                        }
                    }

                    // Failed to shift the shape to the right
                    if ( status != Status.ACTION_OK )
                    {
                        // Rotate back to the previous state
                        if ( rotation == Rotation.CLOCKWISE )
                        {
                            shape.rotateCCW();
                        } else
                        {
                            shape.rotateCW();
                        }
                    }
                    break;
            }

            // Redraw the ui
            repaint();
        }
    }

    
    
    //////////////////////////////////////////////////
    // Player Control Methods
    
    // This method wil move the shape left
    //
    public void moveShapeLeft()
    {
        if ( bIsGameStarted && !bIsGamePaused && !bIsGameOver )
        {
            mShapeStatus = moveShape( mCurrentShape , -1 , 0 );
            updateHelperPosition();
        }
    }

    // This method wil move the shape right
    //
    public void moveShapeRight()
    {
        if ( bIsGameStarted && !bIsGamePaused && !bIsGameOver )
        {
            mShapeStatus = moveShape( mCurrentShape , 1 , 0 );
            updateHelperPosition();
        }
    }

    // This will drop the shape until it reaches the bottom or collide with
    // another block
    //
    public void dropShape()
    {
        if ( bIsGameStarted && !bIsGamePaused && !bIsGameOver )
        {
            Status status = Status.INVALID;

            // Stop the timer
            mTimer.stop();

            do
            {
                // Move the shape down until it is no longer possible
                status = moveShape( mCurrentShape , 0 , 1 );
            } while ( status == Status.ACTION_OK );

            // Record the block position
            mShapeStatus = recordBlocks( mCurrentShape );

            // Record the number of lines removed
            mNumOfLineCleared += removeAllLineFilled();
            accumulateScore();

            // Request for a new block
            requestNewShape();
        }
    }

    
    
    // This method will rotate the current shape clockwise
    //
    public void rotateShapeClockwise()
    {
        if ( bIsGameStarted && !bIsGamePaused && !bIsGameOver )
        {
            rotateShape( Rotation.CLOCKWISE , mCurrentShape );

            rotateShape( Rotation.CLOCKWISE , mHelperShape );
            updateHelperPosition();
        }
    }

    // This method will rotate the current shape clockwise
    //
    public void rotateShapeCounterClockwise()
    {
        if ( bIsGameStarted && !bIsGamePaused && !bIsGameOver )
        {
            rotateShape( Rotation.COUNTER_CLOCKWISE , mCurrentShape );

            rotateShape( Rotation.CLOCKWISE , mHelperShape );
            updateHelperPosition();
        }
    }
    
    // This method will start the game
    //
    public void startGame()
    {
        bIsGameStarted = true;
        
        requestNewShape();
    }

    // This method will pause the game
    //
    public void pauseGame()
    {
        bIsGamePaused = true;

        // Stop the timer
        mTimer.stop();

        // Redraw the screen
        repaint();
    }

    // This method will pause the game
    //
    public void resumeGame()
    {
        bIsGamePaused = false;

        // Resume the timer
        mTimer.start();

        // Redraw the screen
        repaint();
    }

    // This method will reset the game
    //
    public void resetGame()
    {
        // Reset the blocks on the grid
        resetGrid();

        // Remove the current block and state
        mCurrentShape = null;
        mShapeStatus = Status.INVALID;

        // Reset the game speed
        mSpeed = 800;

        // Reset the timer
        if ( mTimer != null )
        {
            mTimer.stop();
            mTimer = null;
        }

        // Reset game state
        bIsGameOver = false;
        bIsGamePaused = false;

        // Restart the game
        requestNewShape();

        // Redraw the screen
        repaint();
    }
}
