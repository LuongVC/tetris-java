/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import tetris.classes.BaseShape;
import tetris.classes.Preferences;
import tetris.interfaces.ITetrisGamePanelListener;
import tetris.interfaces.ITetrisSidePanelListener;

public class TetrisGame
        implements ITetrisGamePanelListener, ITetrisSidePanelListener
{
    //////////////////////////////////////////////////
    // Data Members
    
    private JFrame mMainFrame;
    private TetrisGamePanel mGamePanel;
    private TetrisSidePanel mSidePanel;
    
    
    
    //////////////////////////////////////////////////
    // Constructor
    
    public TetrisGame()
    {
        init();
        
        createGameWindow();
        
        registerListeners();
        registerCallback();
        
        mMainFrame.requestFocus();
    }
    
    
    
    //////////////////////////////////////////////////
    // Custructor Helper Methods
    
    // This method will initialize the data members
    //
    private void init()
    {
        mMainFrame = null;
        mGamePanel = null;
        mSidePanel = null;
    }
    
    // This method will register the listeners
    //
    private void registerListeners()
    {
        mMainFrame.addKeyListener( new TetrisKeyAdapter() );
    }
    
    // This method will register any callbacks
    //
    private void registerCallback()
    {
        if( mGamePanel != null )
        {
            mGamePanel.setCallback( this );
        }
        
        if( mSidePanel != null )
        {
            mSidePanel.setCallback( this );
        }
    }
    
    // This method will create the game window
    //
    private void createGameWindow()
    {
        Color bgColour = Color.DARK_GRAY;
        Color fgColour = Color.WHITE;
        int margin = 10;
        
        // Create the game window
        mMainFrame = new JFrame("Tetris");
        mMainFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        mMainFrame.setLayout( new BorderLayout() );
        
        // Create the game panel where the game is played
        mGamePanel = new TetrisGamePanel( Preferences.GridColumn , Preferences.GridRow );
        mGamePanel.setBorder( BorderFactory.createLineBorder( bgColour ) );
        
        // Create the side panel to display the next shape preview and score
        mSidePanel = new TetrisSidePanel();
        mSidePanel.setBackground( bgColour );
        mSidePanel.setFontColor( fgColour );
        mSidePanel.setFontSize( 32 );
        mSidePanel.setSize( 500 , mGamePanel.getHeight() );
        mSidePanel.setBorder( BorderFactory.createEmptyBorder( margin , margin , margin , margin ) );
        
        // Add the ui panels to the game window
        mMainFrame.add( mGamePanel , BorderLayout.LINE_START );
        mMainFrame.add( mSidePanel , BorderLayout.CENTER );
                
        // Package the UIs
        mMainFrame.pack();
        
        // Calculate window size
        int titleBarHeight = mMainFrame.getInsets().top;
        int width = mGamePanel.getWidth() + Preferences.SidePanelWidth; // mSidePanel width changes base on window width
        int height = mGamePanel.getHeight() + titleBarHeight;
        
        // Disable resizing of the window
        mMainFrame.setSize( width , height );
        mMainFrame.setResizable( false );
    }
    
    
    
    //////////////////////////////////////////////////
    // Protected Methods
    
    protected void resetGame()
    {
        mSidePanel.resetScore();
        mGamePanel.resetGame();
    }
    
    
    
    //////////////////////////////////////////////////
    // Public Methods
    
    // This method will display the game window
    //
    public void showWindow()
    {
        mMainFrame.setVisible( true );
    }
    
    
    
    //////////////////////////////////////////////////
    // Callback Methods

    @Override
    public BaseShape onGetNextShape()
    {
        BaseShape shape = null;
        
        if( mSidePanel != null )
        {
            // Get the next shape
            shape = mSidePanel.getNextShape();
            
            // Create the next new shape
            mSidePanel.createNextShape();
        }
        
        return shape;
    }

    @Override
    public void onAccumlateScore( int numOfLinesCleared )
    {
        if( mSidePanel != null )
        {
            mSidePanel.accumulateScore( numOfLinesCleared );
        }
    }

    @Override
    public int onGetGameSpeed()
    {
        int speed = -1;
        
        if( mGamePanel != null )
        {
            speed = mGamePanel.getSpeed();
        }
        
        return speed;
    }

    @Override
    public void onSetGameSpeed( int speed )
    {
        if( mGamePanel != null )
        {
            mGamePanel.setSpeed( speed );
        }
    }
    
    
    
    //////////////////////////////////////////////////
    // Nested Class
    
    public class TetrisKeyAdapter extends KeyAdapter
    {
        @Override
        public void keyPressed( KeyEvent e )
        {
            if( mGamePanel.isGameStarted() )
            {
                switch( e.getKeyCode() )
                {
                    case KeyEvent.VK_LEFT:
                    case KeyEvent.VK_A:
                        mGamePanel.moveShapeLeft();
                        break;
                    case KeyEvent.VK_RIGHT:
                    case KeyEvent.VK_D:
                        mGamePanel.moveShapeRight();
                        break;
                    case KeyEvent.VK_DOWN:
                    case KeyEvent.VK_S:
                        mGamePanel.dropShape();
                        break;
                    case KeyEvent.VK_Q:
                        mGamePanel.rotateShapeCounterClockwise();
                        break;
                    case KeyEvent.VK_SPACE:
                    case KeyEvent.VK_E:
                        mGamePanel.rotateShapeClockwise();
                        break;
                    case KeyEvent.VK_P:
                    case KeyEvent.VK_PAUSE:
                    case KeyEvent.VK_W:
                        if( mGamePanel.isGamePaused() )
                        {
                            mGamePanel.resumeGame();
                        }
                        else
                        {
                            mGamePanel.pauseGame();
                        }
                        break;
                    case KeyEvent.VK_Y:
                        if( mGamePanel.isGameOver() )
                        {
                            resetGame();
                        }
                        break;
                    case KeyEvent.VK_N:
                        if( mGamePanel.isGameOver() )
                        {
                            System.exit( 0 );
                        }
                        break;
                    case KeyEvent.VK_H:
                        if( !mSidePanel.isDisplayingHelpPanel() )
                        {
                            mSidePanel.displayGameHelpPanel();
                        }
                        else
                        {
                            mSidePanel.displayGameSidePanel();
                        }
                        break;
                }
            }
            else
            {
                switch( e.getKeyCode() )
                {
                    case KeyEvent.VK_CONTROL:
                    case KeyEvent.VK_ALT:
                    case KeyEvent.VK_SHIFT:
                    case KeyEvent.VK_ESCAPE:
                        // Do nothing
                        break;
                    default:
                        mGamePanel.startGame();
                        mSidePanel.displayGameSidePanel();
                        break;
                }
            }
        }
    }
}
