/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.interfaces;

import tetris.classes.BaseShape;

/**
 *
 * @author Luong
 */
public interface ITetrisGamePanelListener
{
    public BaseShape onGetNextShape();
    public void onAccumlateScore( int numOfLinesCleared );
}
