/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tetris.classes.BaseShape;
import tetris.classes.Preferences;
import tetris.classes.TetrisBlockGenerator;
import tetris.interfaces.ITetrisSidePanelListener;

public class TetrisSidePanel extends JPanel
{
    //////////////////////////////////////////////////
    // Data Member
    
    private int mNumOfRows;
    private int mScore;
    private int mInitialGameSpeed;
    
    private double mModifier;
    private double mModiferIncrement;
    
    private BaseShape mNextShape;
    
    private ITetrisSidePanelListener mCallback;
    
    
    
    //////////////////////////////////////////////////
    // UI
    
    private JLabel mScoreTextLabel;
    private JLabel mScoreValueLabel;
    private JLabel mNextTextLabel;
    private TetrisShapePanel mShapePanel;
    
    private JLabel mHelpHeaderLabel;
    private JLabel mHelpMoveLeftLabel;
    private JLabel mHelpMoveRightLabel;
    private JLabel mHelpRotateCWLabel;
    private JLabel mHelpRotateCCWLabel;
    private JLabel mHelpDropShapeLabel;
    private JLabel mHelpPauseGameLabel;
    
    private JLabel mHelpShowLabel;
    private JLabel mHelpHideLabel;
    private boolean bIsDisplayingHelp;
    
    
    
    //////////////////////////////////////////////////
    // Constructor
    
    public TetrisSidePanel()
    {
        init();
        
        setupHelpPanelComponent();
        
        createNextShape();
    }
    
    
    
    //////////////////////////////////////////////////
    // Constructor Helper
    
    // This method will initialize the data members
    //
    private void init()
    {
        mNumOfRows          = 0;
        mScore              = 0;
        mInitialGameSpeed   = -1;
        
        mModifier           = 0.0;
        mModiferIncrement   = 0.05;
        mNextShape          = null;
        
        mCallback           = null;
        
        mScoreTextLabel     = new JLabel( "Score" );
        mScoreValueLabel    = new JLabel( "0" );
        
        mNextTextLabel      = new JLabel( "Next Block" );
        mShapePanel         = new TetrisShapePanel();
        
        mHelpHeaderLabel    = new JLabel( "Controls" );
        mHelpMoveLeftLabel  = new JLabel( "Move Left: [A] or [Left]" );
        mHelpMoveRightLabel = new JLabel( "Move Right: [D] or [Right]" );
        mHelpRotateCWLabel  = new JLabel( "Rotate CW: [E] or [Space]" );
        mHelpRotateCCWLabel = new JLabel( "Rotate CWW: [Q]" );
        mHelpDropShapeLabel = new JLabel( "Drop Down: [S] or [Down]" );
        mHelpPauseGameLabel = new JLabel( "Pause Game: [W] or [Pause]" );
        
        mHelpShowLabel      = new JLabel( "Show Instruction: [H]" );
        mHelpHideLabel      = new JLabel( "Hide Insuction: [H]" );
        bIsDisplayingHelp   = true;
    }
    
    // This method will setup the game SidePanel components
    //
    private void setupSidePanelComponent()
    {
        int horizontalSpacing = 50;
        int veritcalSpacing = 0;
        
        mScoreTextLabel.setAlignmentX( CENTER_ALIGNMENT );
        mScoreValueLabel.setAlignmentX( CENTER_ALIGNMENT );
        mNextTextLabel.setAlignmentX( CENTER_ALIGNMENT );
        mHelpShowLabel.setAlignmentX( CENTER_ALIGNMENT );
        
        removeAll();
        setLayout( new BoxLayout( this , BoxLayout.Y_AXIS ) );
        add( Box.createRigidArea( new Dimension( veritcalSpacing , horizontalSpacing ) ) ); 
        add( mScoreTextLabel );
        add( mScoreValueLabel );
        add( Box.createRigidArea( new Dimension( veritcalSpacing , horizontalSpacing ) ) ); 
        add( mNextTextLabel );
        add( Box.createRigidArea( new Dimension( veritcalSpacing , horizontalSpacing ) ) ); 
        add( mShapePanel );
        add( Box.createRigidArea( new Dimension( veritcalSpacing , horizontalSpacing ) ) ); 
        add( mHelpShowLabel );
    }
    
    // This method will setup the game HelpPanel component
    //
    private void setupHelpPanelComponent()
    {
        int topMarginSpacing = 230;
        int horizontalSpacing = 50;
        int veritcalSpacing = 50;
        
        removeAll();
        setLayout( new BoxLayout( this , BoxLayout.Y_AXIS ) );
        add( Box.createRigidArea( new Dimension( veritcalSpacing , topMarginSpacing ) ) ); 
        add( mHelpHeaderLabel );
        add( mHelpRotateCWLabel );
        add( mHelpRotateCCWLabel );
        add( mHelpDropShapeLabel );
        add( mHelpPauseGameLabel );
        add( Box.createRigidArea( new Dimension( veritcalSpacing , horizontalSpacing ) ) ); 
        add( mHelpHideLabel );
    }
    
    //////////////////////////////////////////////////
    // Setter Methods
    
    public void setFont( Font font , Color colour )
    {
        setFont( font );
        setFontColor( colour );
    }
    
    @Override
    public void setFont( Font font )
    {
        if( mScoreTextLabel != null ) mScoreTextLabel.setFont( font );
        if( mScoreValueLabel != null ) mScoreValueLabel.setFont( font );
        
        if( mNextTextLabel != null ) mNextTextLabel.setFont( font );
        
        if( mHelpHeaderLabel != null ) mHelpHeaderLabel.setFont( font );
        if( mHelpRotateCWLabel != null ) mHelpRotateCWLabel.setFont( font );
        if( mHelpRotateCCWLabel != null ) mHelpRotateCCWLabel.setFont( font );
        if( mHelpDropShapeLabel != null ) mHelpDropShapeLabel.setFont( font );
        if( mHelpPauseGameLabel != null ) mHelpPauseGameLabel.setFont( font );
        
        if( mHelpShowLabel != null ) mHelpShowLabel.setFont( font );
        if( mHelpHideLabel != null ) mHelpHideLabel.setFont( font );
    }
    
    public void setFontColor( Color colour )
    {
        if( mScoreTextLabel != null ) mScoreTextLabel.setForeground( colour );
        if( mScoreValueLabel != null ) mScoreValueLabel.setForeground( colour );
        
        if( mNextTextLabel != null ) mNextTextLabel.setForeground( colour );
        
        if( mHelpHeaderLabel != null ) mHelpHeaderLabel.setForeground( colour );
        if( mHelpRotateCWLabel != null ) mHelpRotateCWLabel.setForeground( colour );
        if( mHelpRotateCCWLabel != null ) mHelpRotateCCWLabel.setForeground( colour );
        if( mHelpDropShapeLabel != null ) mHelpDropShapeLabel.setForeground( colour );
        if( mHelpPauseGameLabel != null ) mHelpPauseGameLabel.setForeground( colour );
        
        if( mHelpShowLabel != null ) mHelpShowLabel.setForeground( colour );
        if( mHelpHideLabel != null ) mHelpHideLabel.setForeground( colour );
    }
    
    public void setFontSize( float size )
    {
        if( mScoreTextLabel != null ) mScoreTextLabel.setFont( 
                mScoreTextLabel.getFont().deriveFont( size ) );
        if( mScoreValueLabel != null ) mScoreValueLabel.setFont( 
                mScoreValueLabel.getFont().deriveFont( size ) );
        
        if( mNextTextLabel != null ) mNextTextLabel.setFont( 
                mNextTextLabel.getFont().deriveFont( size ) );
        
        if( mHelpHeaderLabel != null ) mHelpHeaderLabel.setFont( 
                mHelpHeaderLabel.getFont().deriveFont( size ) );
    }
    
    @Override
    public void setBackground( Color colour )
    {
        super.setBackground( colour );
        
        if( mShapePanel != null )
        {
            mShapePanel.setBackground( getBackground() );
        }
    }
    
    public void setCallback( TetrisGame callback )
    {
        mCallback = callback;
    }
    
    
    
    //////////////////////////////////////////////////
    // Getter Methods
    
    public BaseShape getNextShape()
    {
        return mNextShape;
    }
    
    public boolean isDisplayingHelpPanel()
    {
        return bIsDisplayingHelp;
    }
    
    public boolean isDisplayingGameSidePanel()
    {
        return !bIsDisplayingHelp;
    }
    
    
    
    //////////////////////////////////////////////////
    // Public Methods
    
    // This method will create the next shape in the game
    //
    public void createNextShape()
    {
        // Setup the next shape
        mNextShape = TetrisBlockGenerator.createRandomBlock();
        mShapePanel.setShape( mNextShape );
    }
    
    // This method will tally up the score base on the number of lines cleared
    //
    public void accumulateScore( int numOfLinesCleared )
    {
        if( numOfLinesCleared > 0 )
        {
            // Calculate points
            int newPointsPerRow = (int) ( Preferences.PointsPerRow * (1.0 + ( mModifier * 10.0 ) ) );
            mScore += numOfLinesCleared * newPointsPerRow;

            // Display the poits
            mScoreValueLabel.setText( Integer.toString( mScore ) );

            // Accumulate the number of lines cleared
            mNumOfRows += numOfLinesCleared;
            
            // Update game modifer base on number of line cleared
            mModifier = (mNumOfRows / 10) * mModiferIncrement;

            // Get the initial game speed if it is still not set
            if ( mInitialGameSpeed == -1 )
            {
                if( mCallback != null )
                {
                    mInitialGameSpeed = mCallback.onGetGameSpeed();
                }
                else
                {
                    mInitialGameSpeed = Preferences.DefaultGameSpeed;
                }
            }

            // Calculate new game speed
            int newGameSpeed = (int) ( mInitialGameSpeed - ( mInitialGameSpeed * mModifier ) );
            
            // Make sure that the speed isn't faster than the max game speed
            newGameSpeed = (newGameSpeed < Preferences.MaxGameSpeed) ? Preferences.MaxGameSpeed : newGameSpeed;
            
            // Update game speed
            mCallback.onSetGameSpeed( newGameSpeed );
        }
    }
    
    // This method will reset the scoreboard
    //
    public void resetScore()
    {
        // Reset the score to 0
        mScore = 0;
        
        // Display the poits
        mScoreValueLabel.setText( Integer.toString( mScore ) );
        
        // Start with a different shape
        createNextShape();
    }
    
    public void displayGameSidePanel()
    {
        setupSidePanelComponent();
        bIsDisplayingHelp = false;
        
        // Update the layout being display
        revalidate();
        repaint();
    }
    
    public void displayGameHelpPanel()
    {
        setupHelpPanelComponent();
        bIsDisplayingHelp = true;
        
        // Update the layout being display
        revalidate();
        repaint();
    }
}
